package RefDemo;

import java.lang.reflect.Field;

public class RefEX005 {
	public static void main(String[] args) throws Exception {
	    RefHelper Rfh = new RefHelper();
	    Field[] flds = Rfh.getClass().getDeclaredFields();
	    System.out.printf("There are %d fields\n", flds.length);

	    for (Field f : flds) {
	      System.out.printf("field name = %s type = %s value = %d\n", f.getName(),
	f.getType(), f.getLong(Rfh));
	    }
	  }
}
