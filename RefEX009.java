package RefDemo;

import java.lang.reflect.Field;
import java.lang.reflect.Method;


public class RefEX009 {
	public static void main(String[] args) throws Exception {
	    RefHelper Rfh = new RefHelper();
	    Method[] mthd =Rfh.getClass().getMethods();
	    System.out.printf("There are %d methods\n", mthd.length);

	    for (Method md : mthd) {
	      System.out.printf("method name = %s type = %s parameters = ", md.getName(),
	          md.getReturnType());
	      Class[] types = md.getParameterTypes();
	      for (Class cls : types) {
	        System.out.print(cls.getName() + " ");
	      }
	      System.out.println();
	    }
	  }
}
