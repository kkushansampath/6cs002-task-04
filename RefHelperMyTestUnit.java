package MyTestUnitDemo;

import static MyTestUnit.MyTestUnit.*;

import RefDemo.RefHelper;

public class RefHelperMyTestUnit {
	public static void main(String[] args) {
		try {
		RefHelper s = new RefHelper(5,4, 6);
		checkEquals(s.getX(),5);
		checkEquals(s.getY(),4);
		checkEquals(s.getZ(),6);
		report(); 
		}  catch(ArithmeticException e){System.out.println(e);} 
	}
	  void checkConstructorAndAccess(){
		  try {
		RefHelper s = new RefHelper(3, 4,5);
	    checkEquals(s.getX(), 4);
	    checkEquals(s.getY(), 4);
	    checkEquals(s.getZ(), 4);
	    checkNotEquals(s.getY(), 4);    
	    checkNotEquals(s.getY(), 5); 
	    checkNotEquals(s.getZ(), 5); 
		  }  catch(ArithmeticException e){System.out.println(e);} 
	  }
	  void checkSquareX(){
		  try {
		  RefHelper s = new RefHelper(3, 4,5);
	    s.squareX();
	    checkEquals(s.getX(), 9);
		  } catch(ArithmeticException e){System.out.println(e);} 
	  }
	  void checkSquareY(){
		  try {
		  RefHelper s = new RefHelper(3, 4,5);
	    s.squareY();
	    checkEquals(s.getY(), 16);
		  } catch(ArithmeticException e){System.out.println(e);} 
	  }
	  void checkSquareZ(){
		  try {
		  RefHelper s = new RefHelper(3, 4,5);
	    s.squareZ();
	    checkEquals(s.getZ(), 25);
		  } catch(ArithmeticException e){System.out.println(e);} 
	  }
	  public static void main1(String[] args) {
		  try {
		  RefHelperMyTestUnit ts = new RefHelperMyTestUnit();
	    ts.checkConstructorAndAccess();
	    ts.checkSquareX();
	    ts.checkSquareY();
	    ts.checkSquareZ();
	    report();
		  } catch(ArithmeticException e){System.out.println(e);} 
	  }

}
