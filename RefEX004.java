package RefDemo;

import java.text.Format.Field;

public class RefEX004 {
	public static void main(String[] args) throws Exception {
	    RefHelper Rfh = new RefHelper();
	    java.lang.reflect.Field[] flds = Rfh.getClass().getFields();
	    System.out.printf("There are %d fields\n", flds.length);
	    for (java.lang.reflect.Field fld : flds) {
	      System.out.printf("field name = %s type = %s value = %d\n", fld.getName(),
	    		  fld.getType(), fld.getLong(Rfh));
	    }
	  }
}
