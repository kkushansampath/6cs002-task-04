package RefDemo;

import java.lang.reflect.*;

public class RefEX008 {
	public static void main(String[] args) throws Exception {
	    RefHelper Rfh = new RefHelper();
	    Field[] fields = Rfh.getClass().getDeclaredFields();
	    System.out.printf("There are %d fields\n", fields.length);
	    for (Field fld : fields) {
	      fld.setAccessible(true);
	      long x = fld.getLong(Rfh);
	      x++;
	      fld.setLong(Rfh, x);
	      System.out.printf("field name = %s type = %s value = %d\n", fld.getName(),
	fld.getType(), fld.getLong(Rfh));
	    }
	  }
}
