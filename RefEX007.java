package RefDemo;

import java.lang.reflect.Field;

public class RefEX007 {
	public static void main(String[] args) throws Exception {
	    RefHelper Rfh = new RefHelper();
	    Field[] flds = Rfh.getClass().getDeclaredFields();
	    System.out.printf("There are %d fields\n", flds.length);

	    for (Field fld : flds) {
	      fld.setAccessible(true);
	      System.out.printf("field name = %s type = %s value = %d\n", fld.getName(),
	fld.getType(), fld.getLong(Rfh));
	    }
	  }
}
