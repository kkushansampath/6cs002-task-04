
package RefDemo;

public class RefHelper {

	public long x = 50;
	  private long y = 60;
	  public long z = 70;

	  public RefHelper() {
	  }

	  public RefHelper(long l, long m,long n) {
	    this.x = l;
	    this.y = m;
	    this.z = n;
	  }

	  public void squareX() {
	    this.x *= this.x;
	  }

	  public void squareY() {
	    this.y *= this.y;
	  }
	  
	  public void squareZ() {
		    this.z *= this.z;
		  }

	  public long getX() {
	    return x;
	  }

	  public void setX(long l) {
	    this.x = l;
	  }

	  public long getY() {
	    return y;
	  }

	  public void setY(long m) {
	    this.y = m;
	  }
	  
	  public long getZ() {
		 return z;
		  }

	  public void setZ(long n) {
		 this.z = n;
		  }
	  
	  public String toString() {
	    return String.format("(l:%d, m:%d, n:%d)", x, y,z);
	  }

}

