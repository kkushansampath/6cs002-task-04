package MyTestUnit;

import java.util.*;

public class MyTestUnit {
	private static List<String> checks;
	  private static int checksdone = 0;
	  private static int Checkspassed = 0;
	  private static int Checksfailed = 0;


	  private static void addToReport(String txt) {
		  try {
	    if (checks == null) {
	      checks = new LinkedList<String>();
	    }
	    checks.add(String.format("%04d: %s", checksdone++, txt));
	  } catch(ArithmeticException e){System.out.println(e);} 
	  }

	  
	  public static void checkEquals(long val1, long val2) {
		  try {
	    if (val1 == val2) {
	      addToReport(String.format("  %d == %d", val1, val2));
	      Checkspassed++;
	    } else {
	      addToReport(String.format("* %d == %d", val1, val2));
	      Checksfailed++;
	    }
	  } catch(ArithmeticException e){System.out.println(e);} 
	  }


	  public static void checkNotEquals(long value1, long value2) {
		  try {
	    if (value1 != value2) {
	      addToReport(String.format("  %d != %d", value1, value2));
	      Checkspassed++;
	    } else {
	      addToReport(String.format("* %d != %d", value1, value2));
	      Checksfailed++;
	    }
		  } catch(ArithmeticException e){System.out.println(e);} 
	  }


	  public static void report() {
		  try {
	    System.out.printf("%d checks passed\n", Checkspassed);
	    System.out.printf("%d checks failed\n", Checksfailed);
	    System.out.println();
	    
	    for (String check : checks) {
	      System.out.println(check);
	    }
		  } catch(ArithmeticException e){System.out.println(e);} 
	  }


}
