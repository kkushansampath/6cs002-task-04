package RefDemo;

import java.lang.reflect.*;

public class RefEX010 {
	public static void main(String[] args) throws Exception {
	    RefHelper Rfh = new RefHelper();
	    Method mthd = Rfh.getClass().getDeclaredMethod("setX", long.class);
	    mthd.setAccessible(true);
	    mthd.invoke(Rfh, 86);
	    System.out.println(Rfh);
	  }
}
